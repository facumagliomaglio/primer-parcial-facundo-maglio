﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public class Recibo
    {
        private int numero;

        public int Numero
        {
            get { return numero; }
            set { numero = value; }
        }

        private DateTime mesyano;

        public DateTime MesyAno
        {
            get { return mesyano; }
            set { mesyano = value; }
        }

        private float sueldoBruto;

        public float SueldoBruto
        {
            get { return sueldoBruto; }
            set { sueldoBruto = value; }
        }

        private float sueldoneto;

        public float SueldoNeto
        {
            get { return sueldoneto; }
            set { sueldoneto = value; }
        }

        private float totaldescuento;

        public float TotalDescuento
        {
            get { return totaldescuento; }
            set { totaldescuento = value; }
        }

        private Empleado legajo_empleado;

        public Empleado Legajo_Empleado
        {
            get { return legajo_empleado; }
           set { legajo_empleado = value; }
        }

        private Concepto concepto_lista;

        public Concepto Concepto_lista
        {
            get { return concepto_lista; }
            set { concepto_lista = value; }
        }



    }
}
