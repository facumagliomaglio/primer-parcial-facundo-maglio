﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
   public class Concepto
    {
        private int num;

        public int Num
        {
            get { return num; }
            set { num = value; }
        }

        private string desc;

        public string Desc
        {
            get { return desc; }
            set { desc = value; }
        }

        private float porcentaje;

        public float Porcentaje
        {
            get { return porcentaje; }
            set { porcentaje = value; }
        }

        public override string ToString()
        {
            return Num.ToString();
                
        }

      

       



    }
}
