﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public class Empleado
    {
        private int legajo;

        public int Legajo
        {
            get { return legajo; }
            set { legajo = value; }
        }

        private string nomyape;

        public string NomyApe
        {
            get { return nomyape; }
            set { nomyape = value; }
        }

        private string cuil;

        public string CUIL
        {
            get { return cuil; }
            set { cuil = value; }
        }

        private DateTime fechaAlta;

        public DateTime FechaAlta
        {
            get { return fechaAlta; }
            set { fechaAlta = value; }
        }

        public override string ToString()
        {
            return Legajo.ToString() ;
        }

        

    }
}
