﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Primer_parcial_FM
{
    public partial class Form1 : Form
    {
        BLL.ConceptoBLL gestorConcepto = new BLL.ConceptoBLL();
        BLL.EmpleadoBLL gestorEmpleado = new BLL.EmpleadoBLL();
        BLL.ReciboBLL gestorRecibo = new BLL.ReciboBLL();


       

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            EnlazarEmpleados();
            EnlazarConcepto();
            EnlazarRecibo();
        }

        public void EnlazarEmpleados()
        {

            dataGridView1.DataSource = null;
            dataGridView1.DataSource = gestorEmpleado.Listar();
            

        }
        public void EnlazarConcepto()
        {

            dataGridView3.DataSource = null;
            dataGridView3.DataSource = gestorConcepto.Listar();

        }

        void EnlazarRecibotodos()
        {
            dataGridView2.DataSource = null;
            dataGridView2.DataSource = gestorRecibo.Listar();


        }
        

        void EnlazarRecibo()
        {
            List<BE.Empleado> empleados = gestorEmpleado.Listar();
            comboBox1.DataSource = empleados;
            List<BE.Concepto> conceptos = gestorConcepto.Listar();
            comboBox2.DataSource = conceptos;
            dataGridView2.DataSource = null;
            dataGridView2.DataSource = gestorRecibo.Listar();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(textBox1.Text) || string.IsNullOrWhiteSpace(textBox2.Text) || string.IsNullOrWhiteSpace(textBox1.Text) ||  dateTimePicker1 == null || !EsNumerico(textBox1.Text))
            {
                MessageBox.Show("completar los campos");
            }
            else
            {
                BE.Empleado em = new BE.Empleado();
                // em.Legajo = int.Parse(textBox1.Text);
                em.NomyApe = textBox2.Text;
                em.CUIL = textBox3.Text;
                em.FechaAlta = dateTimePicker1.Value.Date;
                gestorEmpleado.Grabar(em);
                EnlazarEmpleados();
                EnlazarRecibo();

            }

        }

        private void button2_Click(object sender, EventArgs e)
        {

            if (string.IsNullOrWhiteSpace(textBox1.Text))
            {
                MessageBox.Show("completar campo legajo");
            }
            else
            {

                BE.Empleado em = new BE.Empleado();
                em.Legajo = int.Parse(textBox1.Text);

                gestorEmpleado.Eliminar(em);
                EnlazarEmpleados();
                EnlazarRecibo();
            }

        }

        private void button3_Click(object sender, EventArgs e)
        {

            if (string.IsNullOrWhiteSpace(textBox1.Text))
            {
                MessageBox.Show("completar campo legajo");
            }
            else
            {
                BE.Empleado em = new BE.Empleado();
                em.Legajo = int.Parse(textBox1.Text);
                em.NomyApe = textBox2.Text;
                em.CUIL = textBox3.Text;
                em.FechaAlta = dateTimePicker1.Value.Date;

                gestorEmpleado.Grabar(em);
                EnlazarEmpleados();
                EnlazarRecibo();
            }
        }

        

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(textBox6.Text) || string.IsNullOrWhiteSpace(textBox10.Text) || string.IsNullOrWhiteSpace(textBox4.Text) || dateTimePicker2 == null || !EsNumerico(textBox6.Text))
            {
                MessageBox.Show("completar los campos");
            }
            else
            {
                BE.Recibo re = new BE.Recibo();
                re.SueldoBruto = float.Parse(textBox4.Text);
                re.MesyAno = (DateTime)dateTimePicker2.Value.Date;
                re.Legajo_Empleado = (BE.Empleado)comboBox1.SelectedItem;
                re.Concepto_lista = (BE.Concepto)comboBox2.SelectedItem;
                // re.SueldoNeto = float.Parse(textBox7.Text);
                re.TotalDescuento = float.Parse(textBox10.Text);

                gestorRecibo.Grabar(re);

                EnlazarRecibo();
            }

        }

        private void button7_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(textBox5.Text) || string.IsNullOrWhiteSpace(textBox8.Text) || string.IsNullOrWhiteSpace(textBox9.Text) || dateTimePicker1 == null || !EsNumerico(textBox5.Text))
            {
                MessageBox.Show("completar los campos");
            }
            else
            {
                BE.Concepto co = new BE.Concepto();

                co.Desc = textBox8.Text;
                co.Porcentaje = float.Parse(textBox9.Text);

                gestorConcepto.Grabar(co);
                EnlazarConcepto();
                EnlazarRecibo();

            }



        }

        private void button8_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(textBox5.Text) || !EsNumerico(textBox5.Text))
            {
                MessageBox.Show("completar campo numero");
            }
            else
            {

                BE.Concepto co = new BE.Concepto();
                co.Num = int.Parse(textBox5.Text);

                gestorConcepto.Eliminar(co);
                EnlazarConcepto();
                EnlazarRecibo();
            }
        }

        private void button9_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(textBox1.Text) || !EsNumerico(textBox1.Text))
            {
                MessageBox.Show("completar campo numero");
            }
            else
            {

                BE.Concepto co = new BE.Concepto();

                co.Num = int.Parse(textBox5.Text);
                co.Desc = textBox8.Text;
                co.Porcentaje = float.Parse(textBox9.Text);

                gestorConcepto.Grabar(co);
                EnlazarConcepto();
                EnlazarRecibo();
            }
        }

        private void label13_Click(object sender, EventArgs e)
        {

        }

        private void textBox7_TextChanged(object sender, EventArgs e)
        {

        }

        private void label14_Click(object sender, EventArgs e)
        {

        }

        private void textBox10_TextChanged(object sender, EventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)
        {
            /*
            BE.Recibo re = new BE.Recibo();
            re.Legajo_Empleado = ((BE.Empleado)comboBox1.SelectedItem);
            gestorRecibo.Listartodos(re);


            */


        }

        public bool EsNumerico(string texto)
        {
            string numeros = "0123456789";
            bool ok = true;
            int indice = 0;
            do
            {
                Char caracter = texto[indice];
                ok = ok && (numeros.IndexOf(caracter) > -1);
                indice++;
            } while (ok && indice < texto.Length);

            return ok;
        }



    }
}
