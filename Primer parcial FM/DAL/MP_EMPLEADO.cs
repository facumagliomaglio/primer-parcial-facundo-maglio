﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using BE;
namespace DAL
{
    public class MP_EMPLEADO
    {
        Acceso acceso = new Acceso();

        public void  Insertar(BE.Empleado empleado)
        {
            acceso.Abrir();
            List<IDbDataParameter> parametros = new List<IDbDataParameter>();
            parametros.Add(acceso.CrearParametro("@NOM", empleado.NomyApe));
            parametros.Add(acceso.CrearParametro("@cuil", empleado.CUIL));
            parametros.Add(acceso.CrearParametro("@Fec", empleado.FechaAlta));
            acceso.Escribir("Insertar_Empleado", parametros);
            acceso.Cerrar();
            
        }

        public void Eliminar (BE.Empleado empleado)
        {

            acceso.Abrir();
            List<IDbDataParameter> parametros = new List<IDbDataParameter>();

            parametros.Add(acceso.CrearParametro("@Legajo", empleado.Legajo));
            acceso.Escribir("Eliminar_Empleado", parametros);


            acceso.Cerrar();


        }


        public void Editar(BE.Empleado empleado)
        {
            acceso.Abrir();
            List<IDbDataParameter> parametros = new List<IDbDataParameter>();
            parametros.Add(acceso.CrearParametro("@Legajo", empleado.Legajo));
            parametros.Add(acceso.CrearParametro("@NOM", empleado.NomyApe));
            parametros.Add(acceso.CrearParametro("@cuil", empleado.CUIL ));
            parametros.Add(acceso.CrearParametro("@Fec", empleado.FechaAlta));
            acceso.Escribir("Modificar_Empleado", parametros);
            acceso.Cerrar();
           
        }

        public List<BE.Empleado> Listar()
        {
            List<BE.Empleado> empleados = new List<BE.Empleado>();
            Acceso acceso = new Acceso();
            acceso.Abrir();
            DataTable tabla = acceso.Leer("Listar_Empleado");
            acceso.Cerrar();

            foreach (DataRow registro in tabla.Rows)
            {
                BE.Empleado empleado = new BE.Empleado();
                empleado.Legajo = int.Parse(registro[0].ToString());
                empleado.NomyApe = registro[1].ToString();
                empleado.CUIL = registro[2].ToString();
                empleado.FechaAlta = (DateTime)registro[3];
                
                empleados.Add(empleado);
            }


            return empleados;
        }







    }
}
