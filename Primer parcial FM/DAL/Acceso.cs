﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
namespace DAL
{
    internal class Acceso
    {


        private SqlTransaction tx;
        private SqlConnection conexion;

        public void Abrir()
        {
            conexion = new SqlConnection(@"Data Source = .\SQLEXPRESS; Initial Catalog= PrimerParcial;  Integrated SEcurity=SSPI");
            conexion.Open();
        }
        public void Cerrar()
        {
            conexion.Close();
            conexion = null;
            GC.Collect();

        }

        private void iniciartx()
        {
            tx = conexion.BeginTransaction();

        }
          

        private SqlCommand CrearComando(string nombreSP, List<IDbDataParameter> parametros = null, CommandType tipo = CommandType.StoredProcedure)
        {
            SqlCommand comando = new SqlCommand(nombreSP, conexion);
            comando.CommandType = tipo;
            if (parametros != null && parametros.Count > 0)
            {
                comando.Parameters.AddRange(parametros.ToArray());
            }


            return comando;

        }

        public int Escribir(string nombreSP, List<IDbDataParameter> parametros)
        {
            SqlCommand comando = CrearComando(nombreSP, parametros);
            int resultado;
            try
            {

                return resultado = comando.ExecuteNonQuery();
            }
            catch
            {
                resultado = -1;
            }

            return resultado;
        }

        public DataTable Leer(string nombreSP, List<IDbDataParameter> parametros = null)
        {

            SqlDataAdapter adaptador = new SqlDataAdapter();
            adaptador.SelectCommand = CrearComando(nombreSP, parametros);
            DataTable tabla = new DataTable();
            adaptador.Fill(tabla);

            return tabla;



        }


        public IDbDataParameter CrearParametro(string nombre, string valor)
        {
            SqlParameter p = new SqlParameter(nombre, valor);
            p.DbType = DbType.String;
            return p;
        }

    

        public IDbDataParameter CrearParametro(string nombre, int valor)
        {
            SqlParameter p = new SqlParameter(nombre, valor);
            p.DbType = DbType.Int32;
            return p;
        }

      

        public IDbDataParameter CrearParametro(string nombre, float valor)
        {
            SqlParameter p = new SqlParameter(nombre, valor);
            p.DbType = DbType.Single;
            return p;
        }


        public IDbDataParameter CrearParametro(string nombre, DateTime valor)
        {
            SqlParameter p = new SqlParameter(nombre, valor);
            p.DbType = DbType.Date;
            return p;
        }

        

        


        

        public int LeerEscalar(string nombreSP, List<IDbDataParameter> parametros)
        {

            SqlCommand comando = CrearComando(nombreSP, parametros);
            int fa;
            try
            {
                fa = int.Parse(comando.ExecuteScalar().ToString());
            }
            catch 
            {
                fa = -1;
            }
           
            return fa;
        }

    

    }
}
