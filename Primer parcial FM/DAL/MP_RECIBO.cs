﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BE;
using System.Data;
using System.Data.SqlClient;
namespace DAL
{
    public class MP_RECIBO
    {


        Acceso acceso = new Acceso();
        public void Insertar(BE.Recibo recibo)
        {
            List<IDbDataParameter> parametros = new List<IDbDataParameter>();

           
            parametros.Add(acceso.CrearParametro("@SueldoB", recibo.SueldoBruto));
            parametros.Add(acceso.CrearParametro("@SueldoN", recibo.SueldoNeto));
            parametros.Add(acceso.CrearParametro("@MA", recibo.MesyAno));
            parametros.Add(acceso.CrearParametro("@totdesc", recibo.TotalDescuento));
            parametros.Add(acceso.CrearParametro("@Concep", recibo.Concepto_lista.ToString()));
            parametros.Add(acceso.CrearParametro("@LegajoE", recibo.Legajo_Empleado.ToString()));
            acceso.Abrir();
            acceso.Escribir("Insertar_Recibo", parametros);
            acceso.Cerrar();

        }

        public List<BE.Recibo> Listar()
        {
            List<BE.Recibo> recibos = new List<BE.Recibo>();
            Acceso acceso = new Acceso();
            acceso.Abrir();
            DataTable tabla = acceso.Leer("Listar_Recibo");
            acceso.Cerrar();

            foreach (DataRow registro in tabla.Rows)
            {
                BE.Recibo recibo = new BE.Recibo();
              
                recibo.Numero = int.Parse(registro[0].ToString());
                recibo.MesyAno = (DateTime)registro[1];
                recibo.SueldoBruto = float.Parse(registro[2].ToString());
                recibo.SueldoNeto = float.Parse(registro[3].ToString());
                recibo.TotalDescuento = float.Parse(registro[4].ToString());
                //recibo.Concepto_lista = (BE.Concepto)registro[5];

                //recibo.Legajo_Empleado = (BE.Empleado)registro[6];

                /*
                List<Concepto> conceptos = new List<Concepto>();
                recibo.Concepto_lista = (from BE.Concepto c in conceptos
                                         where c.Num == int.Parse(registro[5].ToString())
                                         select c).First();



                List<Empleado> empleados = new List<Empleado>();
                recibo.Legajo_Empleado = (from BE.Empleado e in empleados
                                         where e.Legajo == int.Parse(registro[6].ToString())
                                         select e).First();


              
               */
                
                recibos.Add(recibo);
            }


            return recibos;


        }

        /*public void Listartodos( BE.Recibo recibo)
        {
            acceso.Abrir();
            List<IDbDataParameter> parametros = new List<IDbDataParameter>();

            parametros.Add(acceso.CrearParametro("@Legajo", recibo.Legajo_Empleado.Legajo));
            acceso.Leer("Listar_todos_recibos", parametros);


            acceso.Cerrar();



        }

        public void listarporfecha(BE.Recibo recibo)
        {

          
        }

    */
    


    }

}

