﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BE;
using System.Data;
using System.Data.SqlClient;
namespace DAL
{
    public class MP_CONCEPTO
    {
        Acceso acceso = new Acceso();
        public void Insertar(BE.Concepto concepto)
        {
            acceso.Abrir();
            List<IDbDataParameter> parametros = new List<IDbDataParameter>();
            parametros.Add(acceso.CrearParametro("@DES", concepto.Desc));

            parametros.Add(acceso.CrearParametro("@POR", concepto.Porcentaje));
            acceso.Escribir("Insertar_Concepto", parametros);
            acceso.Cerrar();
            
        }
        public void Editar(BE.Concepto concepto)
        {
            acceso.Abrir();
            List<IDbDataParameter> parametros = new List<IDbDataParameter>();
            parametros.Add(acceso.CrearParametro("@Num", concepto.Num));
            parametros.Add(acceso.CrearParametro("@DES", concepto.Desc));

            parametros.Add(acceso.CrearParametro("@POR", concepto.Porcentaje.ToString()));
            acceso.Escribir("Modificar_Concepto", parametros);

            acceso.Cerrar();
           
        }


        public void Eliminar(BE.Concepto concepto)
        {

            acceso.Abrir();
            List<IDbDataParameter> parametros = new List<IDbDataParameter>();

            parametros.Add(acceso.CrearParametro("@Num", concepto.Num));
            acceso.Escribir("Eliminar_Concepto", parametros);


            acceso.Cerrar();


        }



        
        public List<BE.Concepto> Listar()
        {
            List<BE.Concepto> conceptos = new List<BE.Concepto>();
            Acceso acceso = new Acceso();
            acceso.Abrir();
            DataTable tabla = acceso.Leer("Listar_Concepto");
            acceso.Cerrar();

            foreach (DataRow registro in tabla.Rows)
            {
                BE.Concepto concepto = new BE.Concepto();
                concepto.Num = int.Parse(registro[2].ToString());
                concepto.Desc = registro[0].ToString();
                concepto.Porcentaje = float.Parse(registro[1].ToString());
             

                conceptos.Add(concepto);
            }


            return conceptos;
        }
       


    }
}
