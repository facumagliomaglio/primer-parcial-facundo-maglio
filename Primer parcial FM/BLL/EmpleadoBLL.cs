﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using BE;
namespace BLL
{
    public class EmpleadoBLL
    {
        DAL.MP_EMPLEADO mp_empleado = new DAL.MP_EMPLEADO();

        public void Grabar(BE.Empleado empleado)
        {
           
            if (empleado.Legajo == 0)
            {
                mp_empleado.Insertar(empleado);
            }
            else
            {
                mp_empleado.Editar(empleado);
            }
           
        }

        public void Eliminar(BE.Empleado empleado)
        {
            mp_empleado.Eliminar(empleado);

        }

        public List<BE.Empleado> Listar()
        {
            return mp_empleado.Listar();
        }




    }
}
