﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using BE;
namespace BLL
{
    public class ConceptoBLL
    {
        MP_CONCEPTO mp_concepto = new MP_CONCEPTO();
        public void Grabar(BE.Concepto concepto)
        {

            if (concepto.Num == 0)
            {
                mp_concepto.Insertar(concepto);
            }
            else
            {
                mp_concepto.Editar(concepto);
            }

        }

        public void Eliminar(BE.Concepto concepto)
        {
            mp_concepto.Eliminar(concepto);

        }

        public List<BE.Concepto> Listar()
        {
            return mp_concepto.Listar();
        }

    


    }
}
