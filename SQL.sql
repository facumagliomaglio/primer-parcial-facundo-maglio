USE [master]
GO
/****** Object:  Database [PrimerParcial]    Script Date: 29/9/2020 18:04:39 ******/
CREATE DATABASE [PrimerParcial]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'PrimerParcial', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.SQLEXPRESS\MSSQL\DATA\PrimerParcial.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'PrimerParcial_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.SQLEXPRESS\MSSQL\DATA\PrimerParcial_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [PrimerParcial] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [PrimerParcial].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [PrimerParcial] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [PrimerParcial] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [PrimerParcial] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [PrimerParcial] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [PrimerParcial] SET ARITHABORT OFF 
GO
ALTER DATABASE [PrimerParcial] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [PrimerParcial] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [PrimerParcial] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [PrimerParcial] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [PrimerParcial] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [PrimerParcial] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [PrimerParcial] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [PrimerParcial] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [PrimerParcial] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [PrimerParcial] SET  DISABLE_BROKER 
GO
ALTER DATABASE [PrimerParcial] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [PrimerParcial] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [PrimerParcial] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [PrimerParcial] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [PrimerParcial] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [PrimerParcial] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [PrimerParcial] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [PrimerParcial] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [PrimerParcial] SET  MULTI_USER 
GO
ALTER DATABASE [PrimerParcial] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [PrimerParcial] SET DB_CHAINING OFF 
GO
ALTER DATABASE [PrimerParcial] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [PrimerParcial] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [PrimerParcial] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [PrimerParcial] SET QUERY_STORE = OFF
GO
USE [PrimerParcial]
GO
/****** Object:  Table [dbo].[Conceptos]    Script Date: 29/9/2020 18:04:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Conceptos](
	[Descripcion] [varchar](50) NOT NULL,
	[Porcentaje_Aplicado] [float] NOT NULL,
	[Num_Concepto] [int] NOT NULL,
 CONSTRAINT [PK_Conceptos] PRIMARY KEY CLUSTERED 
(
	[Num_Concepto] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Empleado]    Script Date: 29/9/2020 18:04:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Empleado](
	[Legajo] [int] NOT NULL,
	[NomyApe] [varchar](50) NOT NULL,
	[Cuil] [nvarchar](50) NOT NULL,
	[Fecha_Alta] [date] NOT NULL,
 CONSTRAINT [PK_Empleado] PRIMARY KEY CLUSTERED 
(
	[Legajo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Recibo]    Script Date: 29/9/2020 18:04:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Recibo](
	[Num_Recibo] [int] NOT NULL,
	[Mes_Año] [date] NULL,
	[Sueldo_Bruto] [float] NULL,
	[Sueldo_Neto] [float] NULL,
	[Total_Descuentos] [float] NULL,
	[Lista_Conceptos] [int] NULL,
	[Legajo_Empleado] [int] NULL,
 CONSTRAINT [PK_Recibo] PRIMARY KEY CLUSTERED 
(
	[Num_Recibo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[Eliminar_Concepto]    Script Date: 29/9/2020 18:04:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[Eliminar_Concepto]
@Num int
as
begin
delete from Conceptos
where Num_Concepto = @Num

end
GO
/****** Object:  StoredProcedure [dbo].[Eliminar_Empleado]    Script Date: 29/9/2020 18:04:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create proc [dbo].[Eliminar_Empleado]
@Legajo INT
as 
begin
delete from Empleado
where
Legajo= @Legajo
end
GO
/****** Object:  StoredProcedure [dbo].[Insertar_Concepto]    Script Date: 29/9/2020 18:04:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  proc [dbo].[Insertar_Concepto]
@DES varchar(50), @POR float
as
begin
declare @Num int
set @Num = isnull ( (SELECT MAX (Num_concepto) from Conceptos) ,0) +1
Insert into Conceptos (Num_Concepto, Descripcion, Porcentaje_Aplicado)
values (@Num,@DES,@POR)
end
GO
/****** Object:  StoredProcedure [dbo].[Insertar_Empleado]    Script Date: 29/9/2020 18:04:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[Insertar_Empleado]
@NOM varchar(50), @cuil nvarchar(50), @Fec date
as
begin
declare @Legajo int
set @Legajo = isnull ( (SELECT MAX (Legajo) from Empleado) ,0) +1

insert Empleado values (@Legajo,@NOM,@cuil,@Fec)
end
GO
/****** Object:  StoredProcedure [dbo].[Insertar_Recibo]    Script Date: 29/9/2020 18:04:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[Insertar_Recibo]
@SueldoB float, @SueldoN float, @MA date, @totdesc float, @Concep nvarchar(50), @LegajoE nvarchar(50)
as
begin

declare @Num int
set @Num = isnull ( (SELECT MAX (Num_Recibo) from Recibo) ,0) +1
set @SueldoN = @SueldoB - @totdesc

insert Recibo  values (@Num, @MA, @SueldoB, @SueldoN, @totdesc, @Concep, @LegajoE)

end
GO
/****** Object:  StoredProcedure [dbo].[Listar_Concepto]    Script Date: 29/9/2020 18:04:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Listar_Concepto]
as 
begin
select * from Conceptos
end
GO
/****** Object:  StoredProcedure [dbo].[Listar_Empleado]    Script Date: 29/9/2020 18:04:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Listar_Empleado]
as
begin
Select * from Empleado
END
GO
/****** Object:  StoredProcedure [dbo].[Listar_Recibo]    Script Date: 29/9/2020 18:04:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[Listar_Recibo]
as 
begin

select * from Recibo

end
GO
/****** Object:  StoredProcedure [dbo].[Listar_recibo_fecha]    Script Date: 29/9/2020 18:04:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE proc [dbo].[Listar_recibo_fecha]
as
begin
declare @fec date
SELECT Num_Recibo, Mes_Año, Legajo_Empleado from Recibo
where Mes_Año = @fec
end
GO
/****** Object:  StoredProcedure [dbo].[Listar_todos_recibos]    Script Date: 29/9/2020 18:04:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[Listar_todos_recibos]
@leg int
as
begin
select * from Recibo 
Where Legajo_Empleado = @leg
end
GO
/****** Object:  StoredProcedure [dbo].[Modificar_Concepto]    Script Date: 29/9/2020 18:04:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[Modificar_Concepto]
@Num int, @DES varchar(50), @POR float
as
begin
update Conceptos set
Descripcion = @DES, Porcentaje_Aplicado = @POR
where
Num_Concepto = @Num
end
GO
/****** Object:  StoredProcedure [dbo].[Modificar_Empleado]    Script Date: 29/9/2020 18:04:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create proc [dbo].[Modificar_Empleado]
@Legajo INT, @NOM varchar(50), @cuil nvarchar(50), @Fec date
as 
begin
update Empleado set
NomyApe = @NOM,
Cuil = @cuil,
Fecha_Alta = @Fec
where
Legajo= @Legajo
end
GO
USE [master]
GO
ALTER DATABASE [PrimerParcial] SET  READ_WRITE 
GO
